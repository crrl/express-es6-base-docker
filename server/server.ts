import express = require('express');
import cors = require('cors');
import compression = require("compression");
import helmet = require('helmet')
import mongoose = require('mongoose');
require('dotenv').config();

const app: express.Application = express();
const x = mongoose.connect(`mongodb+srv://${process.env.MONGO_USERNAME}:${process.env.MONGO_PASSWORD}@cluster0.6sjgs.gcp.mongodb.net/${process.env.MONGO_DATABASE}?retryWrites=true&w=majority`, {useNewUrlParser: true});

//Middlewares
import middlewares from './middlewares/index';

//Routers
import baseRouters from './routes/base';

app.use(helmet());

app.use(middlewares.example);

app.use(cors({
  origin: ["http://localhost:3001"],
}));

app.use(compression());

app.use(baseRouters)

app.listen(3000, function () {
console.log('App is listening on port 3000!');
});