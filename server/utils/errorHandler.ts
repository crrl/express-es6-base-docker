import express = require('express');

/**
 * 
 * @param error Message sent by the endpoint
 * @param status status of the request
 * @param route Route that made the request
 */
export const errorHandler = (error: String,  status: Number, route: String) => {
  console.log('============ERROR MESSAGE===========');
  console.log(`error: ${error}`);
  console.log(`status: ${status}`);
  console.log(`error: ${route}`);
  console.log('====================================');

}
