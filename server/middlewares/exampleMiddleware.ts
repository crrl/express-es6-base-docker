import * as express from 'express';

export const loggerMiddleware = function (request: express.Request, response: express.Response, next: express.NextFunction) {
  console.log('====================================');
  console.log(`${request.method} ${request.path}`);
  console.log('====================================');
  next();
}
