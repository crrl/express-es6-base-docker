import * as base from '../models/base';

import express = require('express');
const router = express.Router();

router.get('/', base.getBaseUrl);
router.get('/errorTest', base.getBaseUrlWithError);

export = router;